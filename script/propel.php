<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'contacts' => [
                    'adapter' => 'mysql',
                    'classname' => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn' => 'mysql:host=localhost;dbname=contacts',
                    'user' => 'root',
                    'password' => 'root',
                    'attributes' => []
                ]
            ]
        ],
        'general' => [
            'project' => 'contact'
        ],
        'paths' => [
            'projectDir' => '/home/sio/NetBeansProjects/contact',
            'schemaDir' => '/home/sio/NetBeansProjects/contact/script',
            'phpDir' => '/home/sio/NetBeansProjects/contact/module/Contact/src'
        ],
        'runtime' => [
            'defaultConnection' => 'contacts',
            'connections' => ['contacts']
        ],
        'generator' => [
            'defaultConnection' => 'contacts',
            'connections' => ['contacts']
        ]
    ]
];
