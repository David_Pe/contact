<?php 

return array(//TABLEAU DES PARAMETRES
    'router' => array(//ROUTAGE
        'routes' => array( //ensemble de routes
            'rContact' => array(//déclaration d'une route nommé 'rContact'
                'type' => 'Zend\Mvc\Router\Http\Literal', //type de route
                'options' => array(
                    'route' => '/', //url de la route
                    'defaults' => array(
                        //controleur initialisé par défaut
                        'controller' => 'CtrlIndex',
                        //action par defaut[méthode exécutée par le controleur]
                        'action' => 'index', //[méthode -indexAction- ]                        
                    ),
                ),
            ),
            
            
            'rContact-Gestion' =>array(//déclaration d'une route nommé 'rContact-Gestion'
                'type' => 'segment', //type de route
                'option' => array(
                    'route' => '/:action[/:id]', 
                    'defaults' => array(
                        //controleur initialisé par défaut
                        'controller' => 'CtrlIndex',
                        'constraints' => array(
                            'controller' => '[a-aA-Z][a-zA-Z0-9_-]+',
                        //action par defaut[méthode exécutée par le controleur]
                        'action' => '[a-aA-Z][a-zA-Z0-9_-]+', //[méthode -indexAction- ]
                        ),
                    ),
                ),
            ),
        ),
    ),//FIN ROUTAGE
    'controllers' => array(//CONTROLLER
        'invokables' => array(//declaration des controleurs dynamiquement initialisables
            'Ctrlindex' => 'Contact\Controller\IndexController'
            ),
        ),//FIN CONTROLLER
    
    'view_manager' => array(//GESTIONNAIRE DE VUE  
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'contact/index/index' => __DIR__ . '/../view/contact/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),//FIN GESTIONNAIRE DE VUE
); //FIN TABLEAU DES PARAMETRES
    
       
